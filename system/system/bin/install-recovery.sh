#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:67108864:0a0dd681abba8f7f082606d410e620226594917b; then
  applypatch  EMMC:/dev/block/bootdevice/by-name/boot:67108864:f1f44139817df782ca56a895ec6399f4c2816e28 EMMC:/dev/block/bootdevice/by-name/recovery 0a0dd681abba8f7f082606d410e620226594917b 67108864 f1f44139817df782ca56a895ec6399f4c2816e28:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
