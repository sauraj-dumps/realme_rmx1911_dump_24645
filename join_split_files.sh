#!/bin/bash

cat system/system/app/YouTube/YouTube.apk.* 2>/dev/null >> system/system/app/YouTube/YouTube.apk
rm -f system/system/app/YouTube/YouTube.apk.* 2>/dev/null
cat system/system/app/GoogleLatinInput/GoogleLatinInput.apk.* 2>/dev/null >> system/system/app/GoogleLatinInput/GoogleLatinInput.apk
rm -f system/system/app/GoogleLatinInput/GoogleLatinInput.apk.* 2>/dev/null
cat system/system/app/OppoCamera/OppoCamera.apk.* 2>/dev/null >> system/system/app/OppoCamera/OppoCamera.apk
rm -f system/system/app/OppoCamera/OppoCamera.apk.* 2>/dev/null
cat system/system/app/Chrome/Chrome.apk.* 2>/dev/null >> system/system/app/Chrome/Chrome.apk
rm -f system/system/app/Chrome/Chrome.apk.* 2>/dev/null
cat system/system/priv-app/Mms/Mms.apk.* 2>/dev/null >> system/system/priv-app/Mms/Mms.apk
rm -f system/system/priv-app/Mms/Mms.apk.* 2>/dev/null
cat system/system/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> system/system/priv-app/Velvet/Velvet.apk
rm -f system/system/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat system/system/priv-app/Browser/Browser.apk.* 2>/dev/null >> system/system/priv-app/Browser/Browser.apk
rm -f system/system/priv-app/Browser/Browser.apk.* 2>/dev/null
cat system/system/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> system/system/priv-app/GmsCore/GmsCore.apk
rm -f system/system/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat system/system/priv-app/OppoGallery2/OppoGallery2.apk.* 2>/dev/null >> system/system/priv-app/OppoGallery2/OppoGallery2.apk
rm -f system/system/priv-app/OppoGallery2/OppoGallery2.apk.* 2>/dev/null
cat .git/objects/pack/pack-9cbed07b7cfcddfea7d94f1f6073bf7842292474.pack.* 2>/dev/null >> .git/objects/pack/pack-9cbed07b7cfcddfea7d94f1f6073bf7842292474.pack
rm -f .git/objects/pack/pack-9cbed07b7cfcddfea7d94f1f6073bf7842292474.pack.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
